
class audioPage {

    static verifyTitle () 
    {        
        cy.get('h1').should('be.visible')
        cy.get('h1').should('include.text','SBS Hindi News 13 July 2021: More financial support amid extended NSW lockdown ')          
    }

    static verifySubscribeOptions()
    {

        cy.xpath('//span[contains(text(),"SUBSCRIBE")]').click()
        cy.xpath('//a[contains(text(),"APPLE PODCASTS")]').should('be.visible')
        cy.xpath('//a[contains(text(),"GOOGLE PODCASTS")]').should('be.visible')
    }

    static launchAudioPlayer()
    {
        cy.get('span[class="audiotrack__icon audiotrack__icon--play-pause"]').click()
    }
    static verifyPlayerIsLaunched()
    {
        
        cy.get('div[data-module="audio-player_module"]',{timeout:5000}).should('be.visible')
    }

    static checkPlayPausebutton()  
    {
        cy.get('button[data-type="play-pause-button"]').should('be.visible')
        cy.get('span[class="audio-player__icon audio-player__icon--play-pause"]').click()   // video should be paused

        // verification of video pause 

        
    }

    static checkMuteUnmuteButton()
    {
        cy.get('button[data-type="volume-button"]').should('be.visible')
        cy.get('span[class="audio-player__icon audio-player__volume-icon"]').click()
    }

    static checkForward()
    {
        
        cy.get('button[data-type="step-back-button"]').click()
        cy.get('span[class="audio-player__time audio-player__time--elapsed"]').should('include.text','00:00')   // checking time on the timer
         
        // the above action is taken for making the scrub to initian point so that we can verify the forward action

        cy.get('button[data-type="step-forward-button"]').click()
        cy.get('span[class="audio-player__time audio-player__time--elapsed"]').should('include.text','00:20')     // checking time on the timer
    }
    static checkBackword()
    {

        cy.get('button[data-type="step-back-button"]').click()
        cy.get('span[class="audio-player__time audio-player__time--elapsed"]').should('include.text','00:00')    // checking time on the timer
    }

    static verifyLanguageList()
    {
        cy.get('span[class="icon--translation icon"]').click()
        cy.xpath('//div[contains(text(),"English")]').should('be.visible') // language option is visible 
    }

}
export default audioPage