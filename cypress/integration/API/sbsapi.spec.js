describe('Validate the API response object',() =>{

    it('GET - mp3 files', () =>{
        cy.request('GET','https://www.sbs.com.au/guide/ajax_radio_program_catchup_data/language/hindi/location/NSW/sublocation/Sydney')
        .then((response)=>{
            expect(response).to.have.property('status',200)
            
            //the follwing test to get all mp3 files and verify the file name

            for(let i =0; i<=response.body.length;i++){
            expect(response.body[0].archiveAudio.mp3).to.include('mp3')  // validating mp3 file

            }

            expect(response.body.length).to.eq(7)  // validating the array elements(containting mp3 files) 

            })
       
        })
    })