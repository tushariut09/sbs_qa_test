import audioPage from '../../pageObject/audioPage'

const url = Cypress.env('url')   

before(()=>{
    cy.visit(url)
})


describe('Audio player automation',() =>{

   
    it('can verify title of the audio',()=>{
       
        audioPage.verifyTitle()  // subscribe options should be visible 

    })

    it('can verify subscribe options',()=>{
       
        audioPage.verifySubscribeOptions()  // subscribe options should be visible 

    })
       
    it('can verify audio player is launched',()=>{

        audioPage.launchAudioPlayer()  
        audioPage.verifyPlayerIsLaunched()
     })

     it('can verify audio player controls',()=>{

        audioPage.checkPlayPausebutton()  // play / pause button should be visible and clickable
        audioPage.checkMuteUnmuteButton()   // mute / unmute button should be visible and clickable

     })

     it('can verify forward and backword in scrub',()=>{

        audioPage.checkForward()  
        audioPage.checkBackword()

     })

     it('can verify language List',()=>{

        audioPage.verifyLanguageList()  
       

     })
 

})